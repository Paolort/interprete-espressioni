/**
 * Rappresenta un errore di sintassi.
 * Utile per rappresentare all'utente la natura e il luogo dell'errore.
 *
 * @author Paolo Bono S4529439
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class SyntaxError extends Error {

    public static final String SYNTAX_ERROR_MESSAGE = "La sintassi non risulta corretta alla linea ";
    private static final long serialVersionUID = 5496049562310979530L;

    /**
     * Costruisce un errore dato il messaggio di errore e la posizione dell'errore.
     *
     * @param errorMessage il messaggio di errore
     * @param lineNumber   la linea dove si è verificato l'errore.
     * @param charPosition la posizione nella linea dove si è verificato l'errore.
     */
    public SyntaxError(String errorMessage, int lineNumber, int charPosition) {
        super(SYNTAX_ERROR_MESSAGE + lineNumber + " al carattere " + charPosition + ". " + errorMessage);
    }
}
