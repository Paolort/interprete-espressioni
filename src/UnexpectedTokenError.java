/**
 * Rappresenta un errore dove arrivaun token che non è atteso.
 *
 * @author Paolo Bono S4529439
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */

public class UnexpectedTokenError extends Error {

    private static final long serialVersionUID = -7987598884029476269L;
    public static final String UNEXPECTED_TOKEN_ERROR_MESSAGE = "Token inaspettato: ";

    public UnexpectedTokenError(String errorMessage) {
        super(UNEXPECTED_TOKEN_ERROR_MESSAGE + errorMessage);
    }
}
