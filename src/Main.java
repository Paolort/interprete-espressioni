import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Main {
    /**
     * Interpreta un file in input nel linguaggio del programma. Il programma viene
     * eseguito e ne vengono stampati i risultati linea per linea.
     * Se incontra una linea vuota viene saltata.
     *
     * @param args deve contenere come primo parametro il percorso del file da
     *             interpretare.
     * @throws FileNotFoundException vedi {@link FileReader#FileReader(File)}
     * @throws IOException           vedi {@link CustomStringReader#read()}
     */
    public static void main(String[] args) throws IOException {
        /*
         * Lista dei tokens
         */
        List<Token> tokens = new ArrayList<>();
        /*
         * Lista degli statements
         */
        Iterable<VisitableStatement> statementList;
        /*
         * Analizzatore lessicale
         */
        LexicalAnalyzer lex = new LexicalAnalyzer();
        /*
         * Analizzatore sintattico
         */
        SyntaxAnalyzer synt = new SyntaxAnalyzer();
        /*
         * Esegue il codice
         */
        StatementVisitor runner = new StatementRunner(null);
        /*
         * Stampa i risultati dell'eseguzione
         */
        StatementVisitor printer = new StatementResultPrinter();


        if (args.length > 0) {
            File file = new File(args[0]);
            try {
                //SE IL FILE ESISTE E PUÒ ESSERE LETTO
                if (file.exists() && file.canRead()) {
                    BufferedReader fileBuffReader = new BufferedReader(new FileReader(file));
                    int lineNum = 0;
                    String line;
                    try {
                        //LEGGE LINEA PER LINEA
                        while ((line = fileBuffReader.readLine()) != null) {
                            lineNum++;
                            //SE LA LINEA È VUOTA NON CALCOLA NIENTE
                            if (!line.trim().isEmpty()) {

                                //EFFETTUA ANALISI LESSICALE
                                lex.getTokens(line, lineNum, tokens);
                            }
                        }

                        statementList = synt.getStatementList(tokens.iterator());


                        //PER OGNI LINEA O ALBERO SINTATTICO ESEGUE IL CODICE E STAMPA IL RISULTATO
                        for (VisitableStatement stat : statementList) {
                            stat.accept(runner);

                            stat.accept(printer);
                        }
                    }
                    //CATTURA ERRORE E STAMPA MESSAGGIO INCLUDENDO IL NUMERO DI LINEA
                    catch (Error error) {
                        System.out.println("(ERROR " + error.getMessage() + ")");
                    }
                    fileBuffReader.close();
                } else
                    System.err.println("Il file indicato non esiste o non si possiedono i permessi in lettura");
            } catch (SecurityException e) {
                e.printStackTrace();
            }
        } else
            System.err.println("Argomento mancante");

    }


    /*
    public static void main(String[] args) throws IOException {
        String testFile = "/home/paolo/IdeaProjects/interprete-espressioni/tests/input";
        for (int i = 1; i <= 20; i++) {
            String[] testarg = new String[1];
            testarg[0] = testFile + i + ".txt";
            System.out.println("TEST " + i + " " + testarg[0]);
            main2(testarg);
            System.out.println("----------------\n\n");
        }

    }*/


}
