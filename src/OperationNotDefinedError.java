/**
 * Si verifica quando l'operazione in una {@link OperatorExpression} non è definita e si cerca
 * di effettuare azioni per esempio di valutazione
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */

public class OperationNotDefinedError extends Error {

    private static final long serialVersionUID = 1000171587327586832L;
    private static final String OPERATION_NOT_DEFINED_ERROR_MESSAGE = "tipo di operazione non consentita";

    public OperationNotDefinedError() {
        super(OPERATION_NOT_DEFINED_ERROR_MESSAGE);
    }
}
