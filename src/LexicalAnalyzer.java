import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Analizzatore lessicale. Strutturato per lavorare su singole linee deve essere
 * utilizzato linea per linea.
 * <p>
 * Il suo compito è quello di leggere una linea di codice del linguaggio oggetto
 * dell' esercitazione scomporlo in token e aggiungerli a una lista che poi viene ritornata.
 * La lista può essere anche fornita in modo da appendere il contenuto estratto sul fondo.
 * L'analizzatore lessicale controlla che siano rispettate una parte delle regole di produzione
 * del linguaggio in oggetto.
 * <p>
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class LexicalAnalyzer {

    /**
     * Parentesi tonda aperta
     */
    public static final char OPEN_ROUND_BRACKET = '(';
    /**
     * Parentesi tonda chiusa
     */
    public static final char CLOSE_ROUND_BRACKET = ')';
    /**
     * Fine linea
     */
    public static final int END_LINE = -1;
    /**
     * Spazio
     */
    public static final int SPACE = 32;
    /**
     * Tabulazione
     */
    public static final int TABULATION = 9;

    /**
     * Regex dei caratteri ammessi dal linguaggio
     */
    public static final String REGEX_ALLOWED_LANG_CHARS = "[-]*[a-zA-Z0-9()]*";
    /**
     * Regex dei caratteri ammessi per le variabili
     */
    public static final String REGEX_ALLOWED_VAR = "[a-zA-Z]+";
    /**
     * Regex dei caratteri ammessi per i numeri
     */
    public static final String REGEX_ALLOWED_NUM = "[-]*[0-9]+";
    /**
     * Regex delle parole chiave
     */
    public static final String REGEX_ALLOWED_KEYWORD = "GET|SET|ADD|SUB|DIV|MUL";

    private Pattern allowedLangChar;
    private Pattern allowedVarChar;
    private Pattern allowedNumChar;
    private Pattern allowedKeyWord;

    private List<Token> tokens;

    /**
     * Costruisce un LexicalAnalyzer. Prepara i pattern di controllo.
     */
    public LexicalAnalyzer() {
        allowedLangChar = Pattern.compile(REGEX_ALLOWED_LANG_CHARS);

        allowedVarChar = Pattern.compile(REGEX_ALLOWED_VAR);

        allowedNumChar = Pattern.compile(REGEX_ALLOWED_NUM);

        allowedKeyWord = Pattern.compile(REGEX_ALLOWED_KEYWORD);
    }


    /**
     * Permette di ottenere i token di una linea fornita in ingresso. È possibile specificare dove mettere i token.
     *
     * @param line           la linea dalla quale estrarre i tokens
     * @param lineNumber     il numero della linea nel programma
     * @param optionalTokens la lista dove mettere i token. Se nulla ne viene creata una nuova che viene puoi restituita.
     * @return La lista con in più i token estratti o la lista con solo i token estratti
     * @throws SyntaxError se durante l'estrazione dei token vengono incontrati errori di sintassi.
     * @throws IOException @see {@link CustomStringReader#read()}
     */
    public List<Token> getTokens(String line, int lineNumber, List<Token> optionalTokens) throws SyntaxError, IOException {
        if (optionalTokens == null) tokens = new ArrayList<>();
        else tokens = optionalTokens;

        if (line != null) {
            CustomStringReader reader = new CustomStringReader(line);
            tokenize(reader, lineNumber);
            reader.close();
            return tokens;

        } else
            throw new Error("Linea nulla");
    }

    /**
     * Analizza il testo in input tramite lo stream e lo scompone in token. I token
     * vengono aggiunti alla variabile di stato apposita.
     *
     * @param reader lo stream di caratteri da analizzare
     * @throws SyntaxError se sono presenti caratteri non ammessi dal linguaggio
     * @throws SyntaxError se scomposto un token esso non appartiene a nessun tipo
     *                     di token.
     * @throws IOException se lo stream viene chiuso per maggiori info {@link CustomStringReader#read()}
     */
    private void tokenize(CustomStringReader reader, int lineNumber) throws SyntaxError, IOException {
        if (reader != null) {
            StringBuffer buffer;
            int read;
            Matcher charMatcher;
            boolean match;
            boolean endWord;
            boolean endLine = false;

            // Ripete finchè non finisce la linea
            while (!endLine) {
                endWord = false;
                buffer = new StringBuffer();
                // Rimuove gli spazi bianchi fino alla prossima parola
                removeWhiteSpaces(reader);
                // Esegue almeno una volta e finchè non arriva alla fine di una parola o del
                // matching di essa
                do {
                    // Legge il prossimo carattere
                    read = reader.read();
                    switch (read) {

                        // SE VIENE LETTA LA FINE LINEA
                        case END_LINE:
                            endWord = true;
                            endLine = true;
                            reader.getBack();
                            break;

                        // SE VIENE LETTO IL DELIMITATORE APERTO
                        case OPEN_ROUND_BRACKET:
                            if (buffer.length() > 0) {
                                reader.getBack();
                                endWord = true;
                            } else {
                                // Aggiunge il token delimitatore dato che è composto da un solo carattere
                                tokens.add(new Token(Token.Type.SEPARATOR, "(").setPositionAtLine(reader.getCurrentPosition()).setLineNumberOfToken(lineNumber));
                            }

                            break;

                        // SE VIENE LETTO IL DELIMITATORE CHIUSO
                        case CLOSE_ROUND_BRACKET:
                            if (buffer.length() > 0) {
                                reader.getBack();
                                endWord = true;
                            } else {
                                // Aggiunge il token delimitatore dato che è composto da un solo carattere
                                tokens.add(new Token(Token.Type.SEPARATOR, ")").setPositionAtLine(reader.getCurrentPosition()).setLineNumberOfToken(lineNumber));
                            }
                            break;

                        // SE VENGONO LETTI O SPAZIO O TABULAZIONE
                        case TABULATION:
                        case SPACE:
                            endWord = true;
                            break;

                        // SE VIENE LETTO UN CARATTERE CHE NON APPARTIENE AI CASI PRECEDENTI
                        default:
                            // Aggiunge il carattere alla parola attuale
                            buffer.append((char) read);
                    }

                    // CONTROLLA CHE LA PAROLA ATTUALE CONTINUI A RISPETTARE I CARATTERI CONSENTITI
                    // DAL LIGUAGGIO
                    charMatcher = allowedLangChar.matcher(buffer);
                    match = charMatcher.matches();

                    // SE NON RISPETTA LA CONDIZIONE DESCRITTA AL COMMENTO PRECENDENTE E IL
                    // BUFFER NON È VUOTO VIENE LANCIATO UN ERRORE DI SINTASSI
                    if (!match && buffer.length() > 0)
                        throw new SyntaxError(
                                "Carattere non ammesso", lineNumber, reader.getCurrentPosition());
                } while (!endWord && match);

                if (buffer.length() > 0) {
                    String result = buffer.toString();

                    // throws SyntaxError
                    // Ottiene il tipo del token
                    Token token = buildToken(result, reader.getCurrentPosition() - buffer.length(), lineNumber);

                    // aggiunge il token alla collezione e ne imposta la posizione alla linea
                    tokens.add(token);
                }
            }
        }
    }

    /**
     * Ritorna il token costruito su una porzione di testo. Il tipo di token è scelto
     * dando priorità ai tipi di token:
     * 1 - KEYWORD
     * 2 - VARNAME
     * 3 - NUMBER
     *
     * @param text       la stringa di caratteri da cui ricavarne il token
     * @param initialPos la posizione a cui si trova il primo carattere
     *                   del testo sulla linea. Viene inoltre utilizzata quando
     *                   viene lanciato un syntaxError per indicare la
     *                   posizione dell'errore.
     * @param lineNumber il numero della lina di appartenenza del token nel programma
     * @return Il token costruito sul testo passato come parametro e le info di posizione linea e nella linea.
     * @throws SyntaxError se il testo non corrisponde con nessun tipo di token;
     */
    public Token buildToken(String text, int initialPos, int lineNumber) throws SyntaxError {
        Token toRet;
        Matcher matcher = allowedKeyWord.matcher(text);
        if (matcher.matches())
            toRet = new Token(Token.Type.KEYWORD, text);
        else {
            matcher = allowedVarChar.matcher(text);
            if (matcher.matches())
                toRet = new Token(Token.Type.VARIABLE, text);
            else {
                matcher = allowedNumChar.matcher(text);
                if (matcher.matches())
                    toRet = new Token(Token.Type.NUMBER, text);
                else
                    throw new SyntaxError("Tipo di token non ammesso", lineNumber, initialPos);
            }
        }

        toRet.setLineNumberOfToken(lineNumber);
        toRet.setPositionAtLine(initialPos);
        return toRet;
    }

    /**
     * Rimuove tutti gli spazi e tabulazioni finchè non trova un carattere diverso
     * da questi due ultimi.
     *
     * @param reader la sorgente di testo dalla quale rimuovere gli elementi sopra
     *               citati.
     * @throws IOException vedi @CustomStringReader#read().
     */
    private void removeWhiteSpaces(CustomStringReader reader) throws IOException {
        int read = reader.read();
        while (read == ' ' || read == '	')
            read = reader.read();
        reader.getBack();
    }
}
