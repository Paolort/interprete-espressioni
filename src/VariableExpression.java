/**
 * Rappresenta una variabile in un espressione
 */
public class VariableExpression extends Expression {

    /**
     * Il nome della variabile
     */
    private String name;

    /**
     * Inizializza una variabile dato un nome
     *
     * @param id                      il nome della variabile
     * @param startingLine            la linea dove inizia l'espressione
     * @param startingPosizioneInLine la posizione nella linea.
     */
    public VariableExpression(String id, int startingLine, int startingPosizioneInLine) {
        super(startingLine, startingPosizioneInLine);
        this.name = id;
    }

    /**
     * Permette di ottenere il nome impostato alla variabile.
     *
     * @return il nome della variabile
     */
    public String getName() {
        return name;
    }

    /**
     * Implementazione dell'interfaccia visitor.
     * Permette a un visitatore di espressioni di poter valutare questa variabile.
     */
    public void accept(ExpressionVisitor visitor) {
        visitor.visit(this);
    }


}