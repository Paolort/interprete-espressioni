import java.io.IOException;
import java.io.StringReader;

/**
 * Lettore di stringhe carattere per carattere.
 * Questa classe si è resa necessaria per tener conto del carattere attualmente letto dal lettore cosa che
 * {@link StringReader} non offriva. Non si è proceduto ad estendere la classe poichè si sarebbero dovuti poter nascondere tutti i metodi non necessari e che
 * avrebbero potuto creare problemi. Oltretutto i dati necessari non sono di tipo protected e non hanno get quindi non ci si sarebbe potuto accedere ai dati
 * nemmeno estendendo la classe.
 * <p>
 * Il lettore permette di leggere carattere per carattere e all'evenienza di poter tornare indietro di un carattere.
 * Una volta spostati nuovamente in avanti, e quindi letto un carattere, è possibile tornare indietro nuovamente.
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class CustomStringReader {
    /**
     * Il reader della collection utilizzato.
     */
    StringReader reader;

    /**
     * La posizione posizione attuale sullo stream, ovvero quella che sarà letta alla prossima chiamata di {@link} CustomStringReader#read();
     * Equivalente alla posizione di una testina che scorre su un testo.
     */
    private int currentPosition;

    /**
     * Costruisce un Lettore di stringhe sequenziale
     *
     * @param line la stringa contenente la sequenza di carattere o stream sulla quale i metodi lavoreranno.
     * @throws NullPointerException se la stringa passata come parametro è nulla.
     */
    public CustomStringReader(String line) throws NullPointerException {
        if (line != null) reader = new StringReader(line);
        else throw new NullPointerException("Il parametro non deve essere nullo");
        currentPosition = 0;
    }

    /**
     * Permette di sapere quale sarà la posizione del prossimo carattere letto.
     *
     * @return la posizione del prossimo carattere letto.
     */
    public int getCurrentPosition() {
        return currentPosition;
    }

    /**
     * Marca la posizione poi si sposta sul carattere successivo e lo legge.
     * La posizione marcata serve per poter tornare indietro di un passo tramite il metodo {@link CustomStringReader#getBack()}
     *
     * @return il carattere letto altrimenti se è stato raggiunta la fine dello stream(stringa) -1;
     * @throws IOException se lo stream è stato chiuso vedi {@link CustomStringReader#close()}
     */
    public int read() throws IOException {
        reader.mark(1);
        int read = reader.read();
        if (read != -1) {
            currentPosition++;
        }
        return read;
    }

    /**
     * Torna al carattere precedente. Non è possibile tornare indietro più di un carattere. Se questo metodo
     * viene chiamato più di una volta senza che ci siano stati cambiamenti le volte successive alla prima non fa nulla.
     *
     * @throws IOException OException se lo stream è stato chiuso vedi {@link CustomStringReader#close()}
     */
    public void getBack() throws IOException {
        reader.reset();
        currentPosition--;
    }

    /**
     * Chiude lo stream e rilascia le risorse utilizzate dallo stream reader (La stringa)
     * Una volta invocato il metodo non è più possibile utilizzare {@link CustomStringReader#read()} e {@link CustomStringReader#getBack()}
     */
    public void close() {
        this.reader.close();
    }
}
