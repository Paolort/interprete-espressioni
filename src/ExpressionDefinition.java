
/**
 * Rappresenta la definizione di un'espressione ovvero nel linguaggio oggetto di
 * esercitazione corrisponde alla direttiva GET expression.
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class ExpressionDefinition extends Statement {

    /**
     * L'espressione di cui si desidera ottenerne il valore
     */
    private Expression expr;

    /**
     * Inizializza una definizione di espressione attribuibile alla direttiva GET
     *
     * @param expr l'espressione di cui ne si desidera ottenerne il valore
     */
    public ExpressionDefinition(Expression expr) {
        this.expr = expr;
        setLineNumer(expr.getExpressionStartingLine());
    }

    /**
     * Ritorna l'espressione relativa a questa definizione di espressione
     *
     * @return l'espressione relativa a questa definizione
     */
    public Expression getExpr() {
        return expr;
    }

    /**
     * Permette di impostare o sostituire se già impostata l'espressione di riferimento
     *
     * @param expr l'epressione con cui sostituire il vecchio valore(o impostare se nullo)
     */
    public void setExpr(Expression expr) {
        this.expr = expr;
    }

    /**
     * Accetta un visitatore che effettua operazioni sull'oggetto.
     * Le operazioni sono definite dalle implementazioni del visitatore.
     *
     * @param visitor il visitatore da accettare
     */
    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }

}
