/**
 * Il token rappresenta una porzione di testo soddisfacente un determinato pattern.
 * Il pattern soddifatto ne definisce il tipo. Alcuni tipi hanno priorità rispetto ad altri.
 * (Vedi {@link LexicalAnalyzer#buildToken(String, int, int)} che è utile anche per costruire il token)
 *
 * @author Paolo Bono
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 * @see LexicalAnalyzer
 */

public class Token {
    /**
     * Tipo del token
     */
    public Type type;
    /**
     * Stringa corrispondente al valore del token.
     */
    public String value;
    /**
     * Posizione del token nella linea
     */
    private int initialPositionAtLine;
    /**
     * Linea a cui appartiene il token
     */
    private int lineNumberOfToken;

    /**
     * Costruisce un token dato un valore e un tipo.
     *
     * @param type  il tipo da associare al token
     * @param value il valore del token
     */
    public Token(Type type, String value) {
        this.value = value;
        this.type = type;
        initialPositionAtLine = -1;
    }

    /**
     * Permette di ottenere una rappresentazione dell'oggetto tramite una stringa
     *
     * @return la rappresentazione come stringa dell'oggetto
     */
    public String toString() {
        return "TYPE: " + type + " VALUE: " + value;
    }

    /**
     * Permette di ottenere la posizione nella linea del token
     *
     * @return la posizione del primo carattere del token nella linea
     * dalla quale è stato estratto.
     */
    public int getPositionAtLine() {
        return initialPositionAtLine;
    }

    /**
     * Imposta la posizione iniziale alla linea se il valore passato come parametro
     * è maggiore o uguale a zero.
     *
     * @param positionAtLine la posizione del primo carattere del token
     * @return il token con la posizione aggiornata.
     */
    public Token setPositionAtLine(int positionAtLine) {
        if (positionAtLine >= 0) this.initialPositionAtLine = positionAtLine;
        return this;
    }

    /**
     * Permette di ottenere la linea di appartenenza del token
     *
     * @return il numero della linea di appartenenza del token
     */
    public int getLineNumberOfToken() {
        return lineNumberOfToken;
    }

    /**
     * Permette di impostare la linea di appartenenza del token.
     * <b>Deve essere maggiore o uguale a zero</b>
     *
     * @param lineNumberOfToken la linea di appartenenza del token, deve essere maggiore o uguale
     *                          a zero altrimenti non viene impostata
     * @return l'oggetto sulla quale viene invocato il metodo
     */
    public Token setLineNumberOfToken(int lineNumberOfToken) {
        if (lineNumberOfToken >= 0) this.lineNumberOfToken = lineNumberOfToken;
        return this;
    }

    /**
     * Tipi di token previsti dal linguaggio
     */
    enum Type {KEYWORD, SEPARATOR, NUMBER, VARIABLE}

}
