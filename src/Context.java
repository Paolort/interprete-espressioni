import java.util.HashMap;

/**
 * Lo scopo di questa classe è di contenere il legame tra una variabile e il suo
 * valore ovvero permette di ottenere il contesto in un programma.
 *
 * @author Armando Tacchella (Classe presa da esercizio svolto in aula)
 * @version 1.0
 */
public class Context {

    /**
     * Tabella dei simboli è strettamente legata al contesto quindi non sostituibile
     */
    private final HashMap<String, Long> symbolTable;

    /**
     * Inizializza un contesto. Inizialmente non contiene valori
     */
    public Context() {
        symbolTable = new HashMap<>();
    }

    /**
     * Imposta una variabile. Una volta impostata è possibile reperire il suo
     * valore tramite il metodo {@link Context#getVariable(String)}
     *
     * @param id    il nome della variabile
     * @param value il valore della variabile
     */
    public void setVariable(String id, long value) {
        symbolTable.put(id, value);
    }

    /**
     * Permette di ottenere il valore di una variabile
     *
     * @param id il nome della variabile della quale si desidere il valore
     * @return il valore della variabile se è stato precedentemente impostato, null
     * se il valore non è mai stato impostato.
     */
    public Long getVariable(String id) {
        return symbolTable.get(id);
    }
}
