/**
 * Rappresenta un'operazione ovvero un'addizione | sottrazione | moltiplicazione | divisione.
 * Contiene gli operandi e l'operatore.
 *
 * @author Armando Tacchella (Esercitazione svolta in aula)
 * @version 1.0
 */
public class OperatorExpression extends Expression {
    /**
     * Operazione dell'espressione
     */
    private OperatorTag operator;
    /**
     * Operando sinistro
     */
    private Expression left;
    /**
     * Operando destro
     */
    private Expression right;

    /**
     * Costruisce un espressione che rappresenta un'espressione
     *
     * @param op               operazione rappresentata
     * @param left             operando sinistro
     * @param right            operando destro
     * @param startingLine     Linea dove inizia l'espressione
     * @param startingPosition posizione dove inizia l'espressione(nella linea)
     */
    public OperatorExpression(OperatorTag op, Expression left, Expression right, int startingLine, int startingPosition) {
        super(startingLine, startingPosition);
        operator = op;
        this.left = left;
        this.right = right;
    }

    /**
     * Permette di ottenere il tipo di operazione
     *
     * @return il tipo di operazione
     */
    public OperatorTag getOperator() {
        return operator;
    }

    /**
     * Permette di ottenere l'operando sinistro
     *
     * @return l'operando sinistro.
     */
    public Expression getLeftOperand() {
        return left;
    }

    /**
     * Permette di ottenere l'operando destro.
     *
     * @return l'operando destro.
     */
    public Expression getRightOperand() {
        return right;
    }

    /**
     * Accetta un vititatore.
     *
     * @param visitor il visitatore che compie la visita secondo i metodi da lui definiti
     */
    public void accept(ExpressionVisitor visitor) {
        visitor.visit(this);
    }

    /**
     * Tipi di operazioni permessi
     */
    public enum OperatorTag {NOP, ADD, SUB, MUL, DIV}

}
