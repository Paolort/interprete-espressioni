/**
 * Permette ad uno statement di poter essere visitato
 *
 * @author Paolo Bono S4529439
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public interface VisitableStatement {

    /**
     * Permette di accettare un visitatore che effettua operazioni sullo statement
     *
     * @param visitor il visitatore da accettare
     */
    void accept(StatementVisitor visitor);

}
