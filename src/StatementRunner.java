/**
 * Si occupa di "eseguire" il codice analizzato dall'analizzatore di sintassi e posto in un albero sintattico.
 * Il visitatore implemente il pattern visitor.
 *
 * @author Paolo Bono S4529439
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class StatementRunner implements StatementVisitor {

    /**
     * Il contesto nella quale eseguire il codice
     */
    public Context context;

    /**
     * Valutatore di espressioni.
     */
    private ExpressionEvaluator evaluator;

    /**
     * Inizializza un'istanza in grado di eseguire degli statement dato il contesto.
     * Se si deve analizzare un programma composto da più linee di codice,
     * si deve far eseguire il codice dallo stessa istanza in modo che il
     * contesto rimanga aggiornato.
     *
     * @param context il contesto di partenza, o di riferimento. Se nullo ne viene creato uno.
     */
    public StatementRunner(Context context) {
        if (context == null) this.context = new Context();
        else this.context = context;
        this.evaluator = new ExpressionEvaluator(this.context);
    }

    /**
     * Visita uno statement. Se si tratta di una espressione ne calcola il valore altrimenti se
     * si tratta di una definizione di variabile allora calcola il valore dell'espressione e successivamente
     * lo assegna alla variabile.
     *
     * @param toVisit lo statement da visitare
     */
    @Override
    public void visit(Statement toVisit) throws Error {
        if (toVisit instanceof ExpressionDefinition) {
            ExpressionDefinition expression = (ExpressionDefinition) toVisit;
            evaluateExpression(expression);
        } else {
            if (toVisit instanceof VariableDefinition) {
                VariableDefinition varDef = (VariableDefinition) toVisit;
                assignVariable(varDef);
            } else throw new Error("Illegal type of statement passed to StatementRunner");
        }
    }

    /**
     * Valuta il valore di un'espressione
     *
     * @param expressionDefinition la definizione di espressione della quale valutarne il valore.
     */
    public void evaluateExpression(ExpressionDefinition expressionDefinition) {
        Expression expr = expressionDefinition.getExpr();

        expr.accept(evaluator);

        expressionDefinition.result = evaluator.getResult();
    }

    /**
     * Valuta il valore contenuto nell'espressione all'interno della def di variabile e poi
     * lo assegna alla variabile stessa.
     *
     * @param variableDef la definizione di variabile della quale valutarne e poi assegnarne il valore.
     */
    public void assignVariable(VariableDefinition variableDef) {
        VariableExpression var = variableDef.getVariable();
        Expression expr = variableDef.getExpression();

        expr.accept(evaluator);

        variableDef.result = evaluator.getResult();

        this.context.setVariable(var.getName(), variableDef.result);
    }
}
