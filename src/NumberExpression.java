/**
 * Rappresentazione di un'espressione numerica.
 * Permette di gestire i numeri all'interno del codice del programma.
 *
 * @author Armando Tacchella (Dall'esercitazione svolta in aula)
 * @version 1.0
 */
public class NumberExpression extends Expression {
    /**
     * Valore dell'espressione numerica
     */
    private long value;

    /**
     * Costruisce un'espressione numrerica e ne imposta informazioni relative a posizione nel programma
     *
     * @param value                   il valore dell'espressione numerica.
     * @param startingLine            la linea di appartenenza dell'espressione numerica (Dato che sta su una sola linea)
     * @param startingPosizioneInLine la posizione sulla linea
     */
    public NumberExpression(long value, int startingLine, int startingPosizioneInLine) {
        super(startingLine, startingPosizioneInLine);
        setExpressionStartingLine(startingLine);
        this.value = value;
    }

    /**
     * Permette di ottenere il valore dell'espressione
     *
     * @return il valore dell'espressione
     */
    public long getValue() {
        return value;
    }

    /**
     * Accetta un visitatore
     *
     * @param visitor il visitatore che compie la visita secondo i metodi da lui definiti
     */
    public void accept(ExpressionVisitor visitor) {
        visitor.visit(this);
    }

}