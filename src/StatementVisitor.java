/**
 * Definisce l'interfaccia per permettere a uno statement di essere visitato.
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */

public interface StatementVisitor {
    void visit(Statement toVisit);
}
