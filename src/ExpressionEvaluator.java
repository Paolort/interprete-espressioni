import java.util.Stack;

/**
 * Valutatore di espressioni. Si muove ricorsivamente all'interno di un'espressione se composta da più sotto espressioni
 * valutandone il valore una ad una e poi colletivamente. Permette quindi di ottenerne il valore finale a meno di errori.
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class ExpressionEvaluator implements ExpressionVisitor {

	/**
	 * Stack di valutazione viene usato per ottenere i valori delle sotto espressioni.
	 */
	private Stack<Long> evalutationStack;

	/**
	 * Contesto di valutazione. contiene il valore delle variabili precedentemente valutate in statement differenti e
	 * viene usato per immetterci i valori nuovamente calcolati.
	 */
	private Context context;

	/**
	 * Inizializza un nuovo valutatore di espressioni.
	 *
	 * @param context il contesto da utilizzare se nullo ne viene istanziato uno nuovo
	 */
	public ExpressionEvaluator(Context context) {
		if (context == null) this.context = new Context();
		else this.context = context;
		this.evalutationStack = new Stack<>();
	}

	/**
     * Visita di un'espressione di tipo operatore.
     * Valuta ricursivamente l'operando destra e sinistra. Una volta valutati procede ad effettuare l'operazione
     * matematica che dipende dall'operatore dell'espressione.
     *
     * @param toVisit l'espressione da visitare
     */
    @Override
    public void visit(OperatorExpression toVisit) throws OperationNotDefinedError {
        OperatorExpression.OperatorTag op = toVisit.getOperator();
        Expression left = toVisit.getLeftOperand();
        Expression right = toVisit.getRightOperand();

        left.accept(this);
        right.accept(this);

        Long rightVal = this.evalutationStack.pop();
        Long leftVal = this.evalutationStack.pop();

        switch (op) {
			case ADD:
				this.evalutationStack.push(rightVal + leftVal);
				break;
			case DIV:
				if (rightVal == 0) throw new Error("Divisione per zero non consentita");
				this.evalutationStack.push(leftVal / rightVal);
				break;
			case MUL:
				this.evalutationStack.push(rightVal * leftVal);
				break;
			case SUB:
				this.evalutationStack.push(leftVal - rightVal);
				break;
			default:
				throw new OperationNotDefinedError();
        }

    }

    /**
     * Visita di valutazione di un'espressione di tipo variable.
     * Consulta il contesto e ne estrae il valore della variabile. Se la variabile non è stata impostata allora
     * lancia un errore.
     *
     * @param toVisit l'espressione da visitare
     */
    @Override
    public void visit(VariableExpression toVisit) throws SyntaxError {
        Long value = context.getVariable(toVisit.getName());
        if (value == null)
            throw new SyntaxError("Variabile " + toVisit.getName() + " non precedentemente dichiarata", toVisit.getExpressionStartingLine(), toVisit.getStartingPositionAtLine());
        evalutationStack.push(value);

    }

    /**
     * Visita di valutazione di un'espressione rappresentante un numero.
     * Prende il valore della stringa rappresentante un numero e lo converte in un long.
     *
     * @param toVisit l'espressione da visitare
     */
    @Override
    public void visit(NumberExpression toVisit) {
        long value = toVisit.getValue();
		evalutationStack.push(value);

	}

	/**
	 * Permette di ottenere il risultato di una valutazione.
	 *
	 * @return ritorna il risultato di una valutazione.
	 */
	public Long getResult() {
		if (this.evalutationStack != null && !this.evalutationStack.isEmpty()) {
			return this.evalutationStack.pop();
		} else return null;
	}
}
