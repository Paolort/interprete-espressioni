/**
 * Rappresenta una definizione/un assegnamento di variabile(statement).
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public class VariableDefinition extends Statement {

    /**
     * La variabile oggetto di assegnamento
     */
    private VariableExpression variable;
    /**
     * L'espressione da cui ricavarne il valore da assegnare alla variabile
     */
    private Expression expression;

    /**
     * Costruisce un'assegnazione di variabile
     *
     * @param variable la variabile oggetto di assegnamento
     * @param expr     l'espressione da cui ricavarne il valore da assegnare alla variabile
     */
    public VariableDefinition(VariableExpression variable, Expression expr) {
        this.variable = variable;
        this.expression = expr;
    }


    /**
     * Permette di ottenere la variabile oggetto di assegnamento.
     *
     * @return la variabile oggetto di assegnamento.
     */
    public VariableExpression getVariable() {
        return variable;
    }

    /**
     * Permette di ottenere l'espressione della quale se ne vuole assegnare il valore alla
     * variabile.
     *
     * @return l'epressione oggetto di questa assegnazione di variabile.
     */
    public Expression getExpression() {
        return expression;
    }

    /**
     * Accetta un visitatore che effettua operazioni sull'oggetto.
     * Le operazioni sono definite dalle implementazioni del visitatore.
     *
     * @param visitor il visitatore da accettare
     */
    @Override
    public void accept(StatementVisitor visitor) {
        visitor.visit(this);
    }

}
