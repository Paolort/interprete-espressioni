/**
 * Rappresenta un'istruzione nel linguaggio oggetto dell'esercitazione
 *
 * @author Paolo Bono S4529439
 * @version 1.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public abstract class Statement implements VisitableStatement {

    /**
     * Contiene il risultato di una valutazione da parte di un visitatore.
     */
    protected long result;
    /**
     * Numero di linea dove inizia lo statement
     */
    protected int lineNumber;

    public Statement() {
        this.result = 0;
        setLineNumer(-1);
    }

    /**
     * Ritorna il risultato dell'espressione oggetto di assegnazione alla variabile.
     * L'espressione deve però prima essere valutata da un valutatore. (ES: {@link ExpressionEvaluator})
     *
     * @return il valore dell'espressione se valutato precedentemente, null altrimenti.
     */
    public long getExpressionValue() {
        return this.result;
    }

    /**
     * Permette di ottenere la linea di inizio dell statement
     *
     * @return il numero di appartenenza dello statement
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * Permette di impostare una linea di appartenenza dello statement
     *
     * @param lineNumber il numero della linea.
     *                   <b>Deve essere positiva altrimenti viene impostata a -1</b>
     * @return l'oggetto sulla quale viene chiamato il metodo.
     */
    public Statement setLineNumer(int lineNumber) {
        if (lineNumber >= 0) this.lineNumber = lineNumber;
        else this.lineNumber = -1;
        return this;
    }
}
