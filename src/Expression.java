/**
 * Classe di rappresentazione di un'espressione. Contiene informazioni relative alla sua posizione
 * all'interno del programma.
 *
 * @author Paolo Bono S4529439
 * @version 2.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */
public abstract class Expression implements VisitableExpression {
    /**
     * La linea di inizio dell'espressione all'interno del programma
     */
    private int startingLine;
    /**
     * La posizione di inizio dell'espressione nella linea sopra citata
     */
    private int startingPosizioneInLine;

    /**
     * Inizializza un'espressione dato numero della linea dove inizia l'espressione e la sua posizione
     * all'interno della linea
     *
     * @param startingLine            la linea di partenza
     * @param startingPosizioneInLine la posizione nella linea
     */
    public Expression(int startingLine, int startingPosizioneInLine) {
        setExpressionStartingLine(startingLine);
        setStartingPositionAtLine(startingPosizioneInLine);
    }

    /**
     * Permette di ottenere il numero della linea dove inizia l'espressione
     *
     * @return il numero della linea dove incomincia l'epressione
     */
    public int getExpressionStartingLine() {
        return this.startingLine;
    }

    /**
     * Permette di impostare in che linea si trova la prima keyword di un'espressione
     *
     * @param startingLine la linea di partenza dell'espressione. <b>Se minore di zero viene impostata a zero</b>
     * @return l'espressione sulla quale è stata impostata la linea.
     */
    public Expression setExpressionStartingLine(int startingLine) {
        if (startingLine >= 0) this.startingLine = startingLine;
        else this.startingLine = 0;
        return this;
    }

    /**
     * Permette di ottenere la posizione di inizio dell'espressione nella linea d'inizio di essa.
     *
     * @return la pozione di inizio nella linea dell'espressione in oggetto
     */
    public int getStartingPositionAtLine() {
        return startingPosizioneInLine;
    }

    /**
     * Permette di impostare la posizione di inizio nella linea di appartenenza dell'espressione.
     *
     * @param startingPositionAtLine la posizione di inizio dell'espressione.
     *                               <b>Deve essere maggiore o uguale a zero altrimenti viene impostata a zero</b>
     * @return l'espressione su cui viene effettuata l'operazione.
     */
    public Expression setStartingPositionAtLine(int startingPositionAtLine) {
        if (startingPositionAtLine >= 0) this.startingPosizioneInLine = startingPositionAtLine;
        else this.startingPosizioneInLine = 0;
        return this;
    }

}