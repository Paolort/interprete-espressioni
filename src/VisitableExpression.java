public interface VisitableExpression {
    /**
     * Permette ad un'epressione di essere visitata
     *
     * @param visitor il visitatore che compie la visita secondo i metodi da lui definiti
     */
    void accept(ExpressionVisitor visitor);
}
