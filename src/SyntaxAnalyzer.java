import java.util.ArrayList;
import java.util.Iterator;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Analizzatore di sintassi. Questo tipo di oggetto si occupa di analizzare i token estratti da un analizzatore
 * lessicale. Controlla che la sintassi del linguaggio sia corretta ovvero che l'ordine di comparsa delle direttive,
 * gli argomenti delle direttive, e i separatori siano congruenti con le regole del linguaggio.
 * Per direttive si intende nel caso particolare di questo homework: GET SET ADD SUB MUL DIV.
 *
 *
 * <b> Il risultato dell'analisi sintattica è un albero sintattico coerente secondo quanto definito nel
 * diagramma uml e fornito</b>
 *
 * @author Paolo Bono S4529439
 * @version 2.0
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 * @see SyntaxAnalyzer
 * @see SyntaxError
 */
public class SyntaxAnalyzer {

    public static final String OPERATION_SET = "SET";
    public static final String OPERATION_GET = "GET";
    public static final String OPERATION_ADD = "ADD";
    public static final String OPERATION_SUB = "SUB";
    public static final String OPERATION_MUL = "MUL";
    public static final String OPERATION_DIV = "DIV";

    /**
     * Regex che definisce i separatori aperti ovvero quelli che iniziano una nuova espressione o statement
     */
    public static final String REGEX_OPEN_SEPARATORS = "[(]";
    /**
     * Regex che definisce i separatori chiusi ovvero quelli che terminano un'espressione o statement
     */
    public static final String REGEX_CLOSE_SEPARATORS = "[)]";

    /**
     * Accumulatore a pila che serve per tener traccia dell'apertura e chiusura dei separatori.
     */
    private Stack<Character> separators;

    /**
     * Patterns che vengono poi inizializzati nel costruttore sulle regex corrispondenti
     */
    private Pattern openSeparators;
    private Pattern closeSeparators;

    /**
     * Inizializza un analizzatore sintattico. Prepara strutture dati e pattern per
     * il confronto utili all'ottenimento dell'albero sintattico.
     */
    public SyntaxAnalyzer() {
        this.separators = new Stack<>();
        this.openSeparators = Pattern.compile(REGEX_OPEN_SEPARATORS);
        this.closeSeparators = Pattern.compile(REGEX_CLOSE_SEPARATORS);
    }

    /**
     * Permette di ottenere il prossimo token.
     *
     * @param tokens l'iteratore dalla quale ottenere il prossimo token.
     * @return il prossimo token.
     * @throws UnexpectedTokenError non è presente un prossimo token
     */
    public static Token getNextToken(Iterator<Token> tokens) throws UnexpectedTokenError {
        if (tokens.hasNext()) return tokens.next();
        else throw new UnexpectedTokenError("L'ultima espressione non è stata chiusa correttamente");
    }

    /**
     * Ottiene una lista di statement dato un elenco(iteratore) di token.
     *
     * @param tokens i tokens ottenutti dall'analisi lessicale.
     * @return lo statement costruito a partire dai tokens forniti.
     * @throws SyntaxError          se viene incontrato un errore di sintassi durante l'analisi sintattica.
     * @throws UnexpectedTokenError se mancano token aspettati dalle regole di produzione. O se viene incontrato un
     *                              token nullo.
     */
    public Iterable<VisitableStatement> getStatementList(Iterator<Token> tokens) throws SyntaxError, UnexpectedTokenError {
        ArrayList<VisitableStatement> toReturn = new ArrayList<>();
        if (tokens != null) {
            while (tokens.hasNext()) {
                //CONTROLLO PARENTESI APERTA
                Token firstPar = getNextToken(tokens);
                addSeparator(firstPar);
                //---------

                toReturn.add(getStatement(tokens));

                //CONTROLLO PRESENZA DELLA PARENTESI APERTA PRECEDENTEMENTE
                Token lastPar = getNextToken(tokens);
                removeSeparator(lastPar);
            }
            //CONTROLLO PRESENZA DI SPAZZATURA DOPO L'ULTIMA CHIUSURA
            //if(tokens.hasNext())throw new SyntaxError("sono presenti caratteri aggiuntivi dopo la chiusura dell'ultima parentesi. Rimuoverli.");
        }
        return toReturn;
    }

    /**
     * Ottiene un'espressione in modo ricorsivo.
     * Controlla se è un'espressione composta o se è un semplice numero o variabile.
     * Se è un'espressione composta allora chiama getExpression.
     *
     * @param tokens i tokens da analizzare per ottenere l'espressione.
     * @return L'espressione costruita dai token.
     * @throws SyntaxError          Se viene incrontrato un errore di sintassi
     * @throws UnexpectedTokenError Se viene incontrato un token nullo.
     */
    private Expression recursiveExpression(Iterator<Token> tokens) throws SyntaxError, UnexpectedTokenError {
        Token next = getNextToken(tokens);
        if (next.type == Token.Type.SEPARATOR) {

            addSeparator(next);

            Expression toRet = getExpression(tokens);
            next = getNextToken(tokens);

            removeSeparator(next);

            return toRet;
        } else {
            return getNumberOrVar(next);
        }
    }

    /**
     * Aggiunge un separatore alla pila. Tramite questo metodo è possibile impilare i separatori e successivamente verificare
     * anche se i tipi di separatore vengono chiusi e che rispettino l'ordine
     *
     * @param separator il separatore da aggiungere alla pila, deve essere un separatore aperto ovvero
     *                  che inizia un'espressione o direttiva.
     * @throws Error       se il token passato come parametro è un puntatore nullo.
     * @throws SyntaxError se il token passato non è un separatore o è un separatore che chiude un'espressione.
     */
    private void addSeparator(Token separator) throws Error {
        if (separator != null && separator.type == Token.Type.SEPARATOR) {
            Matcher parMatcher = openSeparators.matcher(separator.value);
            if (parMatcher.matches()) this.separators.push(separator.value.charAt(0));
            else
                throw new SyntaxError("Separatore aperto aspettato è stato rilevato un separatore chiuso", separator.getLineNumberOfToken(), separator.getPositionAtLine());
        } else {
            if (separator == null)
                throw new UnexpectedTokenError("token nullo passato al metodo addSeparator().");
            else
                throw new SyntaxError("Separatore aperto atteso", separator.getLineNumberOfToken(), separator.getPositionAtLine());
        }
    }

    /**
     * Rimuove un separatore dalla pila confrontandolo con quello specificato via parametro.
     * Prende il primo sepraratore sulla pila, verifica che sia l'apertura del separatore
     * specificato altrimenti lancia un'eccezioni.
     *
     * @param separator il separatore confrontare, deve essere un separatore chiuso ovvero
     *                  che termina un'espressione o statement.
     * @throws Error       se il token passato come parametro è un puntatore nullo.
     * @throws SyntaxError se il token passato non è un separatore o è un separatore che apre un'espressione.
     *                     Oppure se si tratta di un tipo diverso di separatore
     */
    private void removeSeparator(Token separator) {
        if (separator != null && separator.type == Token.Type.SEPARATOR) {
            Matcher parMatcher = closeSeparators.matcher(separator.value);
            if (parMatcher.matches()) {
                char expected = getOppositeSeparator(this.separators.pop());
                if (!(expected == separator.value.charAt(0)))
                    throw new SyntaxError("Il tipo di separatore chiuso aspettato non coincide", separator.getLineNumberOfToken(), separator.getPositionAtLine());
            } else
                throw new SyntaxError("Separatore CHIUSO atteso. È stato immesso un separatore aperto", separator.getLineNumberOfToken(), separator.getPositionAtLine());
        } else {
            if (separator == null)
                throw new UnexpectedTokenError("token nullo passato al metodo removeSeparator().");
            else
                throw new SyntaxError("Separatore chiuso aspettato", separator.getLineNumberOfToken(), separator.getPositionAtLine());
        }
    }

    /**
     * Permette di ottenere il separatore opposto a quello specificato.
     * Se ad esempio si specifica '(' si ottiene ')' e viceversa.
     *
     * @param separator il separatore della quale ottenerne l'opposto.
     * @return il separatore opposto.
     */
    public char getOppositeSeparator(char separator) {
        switch (separator) {
            case LexicalAnalyzer.OPEN_ROUND_BRACKET:
                return LexicalAnalyzer.CLOSE_ROUND_BRACKET;
            case LexicalAnalyzer.CLOSE_ROUND_BRACKET:
                return LexicalAnalyzer.OPEN_ROUND_BRACKET;
            default:
                return 0;
        }
    }

    /**
     * Dato un token lo converte in un {@link NumberExpression} o in un {@link VariableExpression}
     * in base al contenuto del token. Se il tipo di token non corrisponde a VARIABLE
     * oppure a NUMBER viene lanciato un errore.
     *
     * @param toTurnInto il token da cui ottenere un'espressione numerica o una variabile.
     * @return l'espressione costruita sul token
     * @throws SyntaxError          se il tipo di token non corrisponde a un numero o una variabile.
     * @throws UnexpectedTokenError se il token parametro è nullo
     */
    private Expression getNumberOrVar(Token toTurnInto) throws SyntaxError, UnexpectedTokenError {
        if (toTurnInto != null) {
            if (toTurnInto.type == Token.Type.VARIABLE) {
                return new VariableExpression(toTurnInto.value, toTurnInto.getLineNumberOfToken(), toTurnInto.getPositionAtLine());
            } else if (toTurnInto.type == Token.Type.NUMBER) {
                return new NumberExpression(Long.parseLong(toTurnInto.value), toTurnInto.getLineNumberOfToken(), toTurnInto.getPositionAtLine());
            } else
                throw new SyntaxError(
                        "Una nuova espressione (ADD|SUB|MUL|DIV expression1 expression2), un numero oppure una variabile è prevista in questa posizione", toTurnInto.getLineNumberOfToken(), toTurnInto.getPositionAtLine());
        } else throw new UnexpectedTokenError("token nullo inaspettato");
    }

    /**
     * Ottiene un espressione. Se incontra una sotto espressione allora richiama se stessa ricorsivamente per ottenerne il valore.
     *
     * @param tokens i tokens dalla quale ottenere l'espressione
     * @return L'espressione ottenuta dai tokens.
     * @throws SyntaxError          se viene incontrato un'errore di sintassi
     * @throws UnexpectedTokenError se viene incontrato un token nullo
     */
    private Expression getExpression(Iterator<Token> tokens) throws SyntaxError, UnexpectedTokenError {
        OperatorExpression.OperatorTag op;
        Token keyword = getNextToken(tokens);
        if (keyword.type != Token.Type.KEYWORD)
            throw new SyntaxError("Manca un operatore (Operatori aspettati: ADD|MUL|DIV|SUB)", keyword.getLineNumberOfToken(), keyword.getPositionAtLine());
        else {
            switch (keyword.value) {
                case OPERATION_ADD:
                    op = OperatorExpression.OperatorTag.ADD;
                    break;
                case OPERATION_SUB:
                    op = OperatorExpression.OperatorTag.SUB;
                    break;
                case OPERATION_MUL:
                    op = OperatorExpression.OperatorTag.MUL;
                    break;
                case OPERATION_DIV:
                    op = OperatorExpression.OperatorTag.DIV;
                    break;
                default:
                    throw new OperationNotDefinedError();
            }

            /*
             expr[0] = primo termine dell'espressione
             expr[1] = secondo termine dell'espressione
             */
            Expression[] expr = new Expression[2];

            Token next;

            for (int i = 0; i < 2; i++) {
                next = getNextToken(tokens);
                if (next.type == Token.Type.SEPARATOR) {
                    if (openSeparators.matcher(next.value).matches()) {
                        addSeparator(next);

                        Expression toRet = getExpression(tokens);
                        next = getNextToken(tokens);

                        removeSeparator(next);

                        expr[i] = toRet;
                    } else
                        throw new SyntaxError("Parentesi chiusa non attesa", next.getLineNumberOfToken(), next.getPositionAtLine());
                } else {
                    expr[i] = getNumberOrVar(next);
                }
            }

            return new OperatorExpression(op, expr[0], expr[1], keyword.getLineNumberOfToken(), keyword.getPositionAtLine());

        }

    }

    /**
     * Ottiene uno statement dati i token dalla quale costruirne uno
     *
     * @param tokens i token contenenti le informazioni dello statement
     * @return lo statement costruito dai token.
     * @throws SyntaxError          se viene  incontrato un errore di sintassi.
     * @throws UnexpectedTokenError se viene incontrato un token nullo.
     */
    public Statement getStatement(Iterator<Token> tokens) throws SyntaxError, UnexpectedTokenError {
        Statement toReturn;
        //CONTROLLO PRIMA DIRETTIVA E SE SET CONTROLLO ANCHE ID VARIABILE
        //SE PRESENTE ULTERIORE ESPRESSIONE DELEGA A METODO recursiveExpression()
        Token firstKeyWord = getNextToken(tokens);
        if (firstKeyWord == null) throw new UnexpectedTokenError("token nullo inaspettato");


        if (firstKeyWord.type != Token.Type.KEYWORD)
            throw new SyntaxError("Manca il primo identificatore. (Operatori aspettati: SET|GET)", firstKeyWord.getLineNumberOfToken(), firstKeyWord.getPositionAtLine());

        boolean isSet;

        if ((isSet = firstKeyWord.value.equals(OPERATION_SET)) || (firstKeyWord.value.equals(OPERATION_GET))) {
            VariableExpression variableExpr = null;
            if (isSet) {
                Token varToken = getNextToken(tokens);
                if (varToken == null) throw new UnexpectedTokenError("token nullo inaspettato");
                if (varToken.type == Token.Type.VARIABLE) {
                    variableExpr = new VariableExpression(varToken.value, varToken.getLineNumberOfToken(), varToken.getPositionAtLine());
                } else
                    throw new SyntaxError("Manca o è errato l'identificatore della variabile. (SET varname expression)", varToken.getLineNumberOfToken(), varToken.getPositionAtLine());
            }

            Expression expression = recursiveExpression(tokens);

            if (isSet) {
                toReturn = new VariableDefinition(variableExpr, expression);
            } else {
                toReturn = new ExpressionDefinition(expression);
            }
            toReturn.setLineNumer(firstKeyWord.getLineNumberOfToken());
            return toReturn;
        } else {
            throw new SyntaxError(
                    "La prima espressione deve essere necessariamente un SET o GET. (SET varname expression) (GET expression)", firstKeyWord.getLineNumberOfToken(), firstKeyWord.getPositionAtLine());
        }
    }
}
