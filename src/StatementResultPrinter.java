/**
 * Visitatore che ha il compito di stampare a video il risultato delle operazioni
 * valutate da {@link ExpressionEvaluator}.
 *
 * @author Paolo Bono S4529439
 * @see <a href="https://gitlab.com/Paolort/interprete-espressioni">Progetto su gitlab</a>
 */

public class StatementResultPrinter implements StatementVisitor {

    /**
     * A seconda del tipo di statement si occupa di stampare il risultato di una valutazione
     *
     * @param toVisit lo statement da visitare
     * @throws Error se viene passato un tipo di statement non previsto
     */
    @Override
    public void visit(Statement toVisit) throws Error {
        if (toVisit instanceof ExpressionDefinition) {
            System.out.println(toVisit.getExpressionValue());
        } else {
            if (toVisit instanceof VariableDefinition) {
                VariableDefinition def = (VariableDefinition) toVisit;
                System.out.println(def.getVariable().getName() + " = " + toVisit.getExpressionValue());
            } else throw new Error("Illegal type of statement passed to StatementResultPrinter");
        }
    }
}
