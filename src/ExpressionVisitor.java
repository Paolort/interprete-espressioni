/**
 * Definisce un visitatore di espressioni in particolar modo cosa
 * deve poter visitare.
 *
 * @author Armando Tacchella (Esercitazione svolta in aula
 * @version 1.0
 */
public interface ExpressionVisitor {
    /**
     * Rende possibile la visita a un'espressione numerica
     *
     * @param toVisit l'espressione da visitare
     */
    void visit(NumberExpression toVisit);

    /**
     * Rende possibile la visita a una variabile
     *
     * @param toVisit l'espressione da visitare
     */
    void visit(VariableExpression toVisit);

    /**
     * Rende possibile la visita a un'espressione composta:
     * addizione, sottrazione, divisione, moltiplicazione.
     *
     * @param toVisit l'espressione da visitare
     */
    void visit(OperatorExpression toVisit);
}